#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:29773840:d739bd1b81f982502512725efaa907e8eba66bea; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/13540000.dwmmc0/by-name/BOOT:20625424:0c242c0a93eab4c73fceb0bb7df4472276b8c48d \
          --target EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:29773840:d739bd1b81f982502512725efaa907e8eba66bea && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
